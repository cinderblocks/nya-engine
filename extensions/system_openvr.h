//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

#include "scene/material.h"
#include "render/fbo.h"
#include "render/vbo.h"
#include "math/quaternion.h"
#include <openvr.h>

namespace nya_system
{
    
bool disable_vsync();

vr::EVRCompositorError submit_texture(vr::EVREye eye,const nya_render::texture &tex,vr::EColorSpace colorspace=vr::ColorSpace_Gamma);
vr::EVRCompositorError submit_texture(vr::EVREye eye,const nya_scene::texture &tex,vr::EColorSpace colorspace=vr::ColorSpace_Gamma);
vr::EVRCompositorError submit_texture(vr::EVREye eye,const nya_scene::texture_proxy &tex,vr::EColorSpace colorspace=vr::ColorSpace_Gamma);

nya_math::mat4 get_proj_matrix(vr::IVRSystem *hmd,vr::EVREye eye, float znear, float zfar);

nya_math::vec3 get_pos(const vr::HmdMatrix34_t &mat);
nya_math::quat get_rot(const vr::HmdMatrix34_t &mat);
nya_math::quat get_camera_rot(const vr::HmdMatrix34_t &mat);

nya_render::vbo create_distortion_mesh(vr::IVRSystem *hmd,vr::EVREye eye,bool flip_y=false,int segments_count=43);

class vr_postprocess
{
public:
    bool init(vr::IVRSystem *hmd);
    const nya_scene::texture &process(vr::EVREye eye,const nya_scene::texture &tex);
    const nya_scene::texture &process(vr::EVREye eye,const nya_scene::texture_proxy &tex);
    int get_width() const { return m_result_tex_l.get_width(); }
    int get_height() const { return m_result_tex_l.get_height(); }
    void release();

private:
    nya_render::fbo m_fbo_l,m_fbo_r;
    nya_render::vbo m_mesh_l, m_mesh_r;
    nya_scene::material m_mat;
    nya_scene::texture_proxy m_tex;
    nya_scene::texture m_result_tex_l,m_result_tex_r;
};

}
